<a name="readme-top"></a>
<br />

<div align="center">
<a href="https://baro-migraine-meter.vercel.app/">
<img src="https://gitlab.com/Emerald-Typed/readme/-/raw/main/public/LOGO.png" alt="Logo">
</a>
<h3 align="center">Barometric Pressure Migraine Monitor</h3>
<p align="center">
Kyle Hipple - Emerald Typed || Simple Barometric Pressure Monitor
<br />
<a href="https://baro-migraine-meter.vercel.app/"><strong>View Deployment  »</strong></a>
<br />
<a href="https://gitlab.com/Emerald-Typed/baro_migraine_meter"><strong>Explore the Repo »</strong></a>
<br />
<a href="https://emerald-typed.vercel.app/contact">Contact Me</a>
·
<a href="https://gitlab.com/Emerald-Typed/baro_migraine_meter/issues">Report Bug</a>
·
<a href="https://gitlab.com/Emerald-Typed/baro_migraine_meter/issues">Request Feature</a>
</p>
</br>

[![Last Commit][commit-badge]](https://gitlab.com/Emerald-Typed/baro_migraine_meter)

</div>

## Table of Contents

#### [About](#about-the-project)

#### [ScreenShot](#screenshot)

#### [Features](#features)

#### [Libraries](#built-with)

#### [Usage](#usage)

#### [Contact](#contact)

#### [License](#license)

## About The Project

### ScreenShot

![Project Screen Shot][ScreenShot]

Welcome to my Baro-Migraine-Meter

This is a simple React app designed to monitor barometric pressure fluctuations over 24 hours and store the data in cookies. It's specifically tailored to provide useful information for migraine sufferers, helping them track pressure changes and anticipate potential triggers.

</br>

## Features

- **24-hour Pressure Monitoring:** Scrapes barometric pressure data over 24 hours from time of request.
- **Cookie-Based Data Storage:** Stores pressure data in cookies instead of a database, reducing costs and ensuring quick access(also save me money).
- **Migraine Insights:** Provides helpful data visualization and insights for migraine sufferers to manage their condition effectively.
- **useFormState and Newest Next.js Server Actions:** Employ useFormState and the latest Next.js server actions for optimized form handling and improved server-side functionality.
- **React Recaptcha v2:** Enhance security with React Recaptcha v2, ensuring a secure and reliable user experience by preventing spam and abuse.

</br>

## Libraries

### Built With

[![Next.js](https://img.shields.io/badge/next.js-34d399?logo=next.js&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://nextjs.org/)
[![React](https://img.shields.io/badge/react-34d399?logo=react&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://reactjs.org/)
[![TypeScript](https://img.shields.io/badge/typescript-34d399?logo=typescript&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://www.typescriptlang.org/)
[![Tailwind](https://img.shields.io/badge/tailwindcss-34d399?logo=tailwindcss&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://tailwindcss.com/)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Usage

Welcome to the Barometric Pressure Migraine Monitor

1. **Visit the Deployed Page:**
   Open your browser and navigate to the [Barometric Pressure Migraine Monitor](https://baro-migraine-meter.vercel.app/) 📊

2. **View Pressure Data:**
   Explore the app to view real-time barometric pressure data and insights tailored for migraine sufferers.

3. **Contact Me:**
   For any inquiries or feedback, visit the [Contact Page](https://emerald-typed.vercel.app/contact) 📞

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Roadmap: Remove Nested UI's after V0 2/26 update

**Implement Graph Feature:** Develop a graph feature to visualize pressure fluctuations based on the 24CastCookie. This feature will provide users with a clear graphical representation of barometric pressure changes over time. Progress has already begun, and a new branch will be created for further development.
- **Develop Forecast Functionality:** Work on implementing forecast functionality to provide users with future pressure predictions. This feature will enhance the app's utility by allowing users to anticipate upcoming pressure changes and plan accordingly.
- **Separate Cookies by Zipcode and Time:** Revise the cookie storage mechanism to combine name of cookie to Data-{zipcode}-{time}. Utilizing a naming convention like Data-{zipcode}-{time} will ensure that pressure data is stored uniquely for each hour, preventing overwriting zipcodes and enabling accurate tracking of pressure fluctuations.

  </br>

## Contact

Kyle Hipple - [Emerald-Typed Gitlab](https://gitlab.com/Emerald-Typed) - [Contact Me](https://emerald-typed.vercel.app/contact)

Project Link: [https://gitlab.com/Emerald-Typed/baro_migraine_meter](https://gitlab.com/Emerald-Typed/baro_migraine_meter)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## License

All Rights Reserved License. See [License.txt](https://gitlab.com/Emerald-Typed/baro_migraine_meter/-/raw/main/LICENSE.txt)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

[commit-badge]: https://img.shields.io/gitlab/last-commit/Emerald-Typed/baro_migraine_meter?label=last%20commit&style=for-the-badge&color=34d399&labelColor=%23a1a1aa
[ScreenShot]: https://gitlab.com/Emerald-Typed/baro_migraine_meter/-/raw/main/public/barometrics.png
[NPM]: https://img.shields.io/badge/npm-34d399?logo=npm&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge
[NPM-url]: https://www.npmjs.com/
[Node.js]: https://img.shields.io/badge/node.js-34d399?logo=node.js&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge
[Node-url]: https://nodejs.org/en/
[React.js]: https://img.shields.io/badge/react-34d399?logo=react&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge
[React-url]: https://reactjs.org/
