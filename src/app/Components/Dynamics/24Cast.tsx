"use client";
import { DateFormatter } from "@/app/Components/Helpers/DateFormatter";
import { useZipCode } from "@/app/Components/Dynamics/ZipContext";
import { getTwentyFourCast } from "@/app/server/cookies";
import { useState, useEffect } from "react";
import { twentyFourCast } from "@/app/Components/dataCleanup/types";
import { Line } from "react-chartjs-2";
import "chart.js/auto";

export const options = {
  responsive: true,
  plugins: {
    legend: {
      position: "top" as any,
    },
    title: {
      display: true,
      text: "Pressure over 24 Hours",
    },
  },
};
export const ForecastGraph = () => {
  const [timeValues, setTimeValues] = useState<string[]>([]);
  const [pressureValues, setPressureValues] = useState<number[]>([]);
  const { selectedZipCode } = useZipCode();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const fetchedData: twentyFourCast = await getTwentyFourCast(
          selectedZipCode as string
        );
        if (fetchedData && fetchedData.length > 0) {
          const times = fetchedData.map((data) => DateFormatter(data.time));
          const pressures = fetchedData.map(
            (data) => data.pressureSurfaceLevel
          );
          setTimeValues(times);
          setPressureValues(pressures);
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, [selectedZipCode]);

  const RenderChart = () => {
    const data = {
      labels: timeValues,
      datasets: [
        {
          label: "Pressure",
          data: pressureValues,
          borderColor: ["rgb(46, 204, 113)"],
          backgroundColor: [
            "rgba(46, 204, 113, 0.2)", // Emerald green with 20% opacity
            "rgba(155, 89, 182, 0.2)", // Purple with 20% opacity
          ],
          fill: true,
        },
      ],
    };

    return <Line options={options} data={data} />;
  };

  return (
    <div className="w-full flex flex-col items-center">
      <RenderChart />
    </div>
  );
};

export default ForecastGraph;
