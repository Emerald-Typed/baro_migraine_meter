"use client";
import { DateFormatter } from "@/app/Components/Helpers/DateFormatter";
import { useZipCode } from "@/app/Components/Dynamics/ZipContext";
import { getData } from "@/app/server/cookies";
import { useState, useEffect } from "react";
import { MeterData } from "../dataCleanup/types";

function Meter() {
  const [data, setData] = useState<MeterData | null>(null);
  const { selectedZipCode } = useZipCode();
  useEffect(() => {
    const fetchData = async () => {
      const fetchedData: MeterData = await getData(selectedZipCode as string);
      setData(fetchedData);
    };
    fetchData();
  }, [selectedZipCode]);
  // @ts-ignore
  const HPAdifferntial = data?.high - data?.low;

  let value;
  if (HPAdifferntial > 10) {
    value = 100;
  } else {
    value = HPAdifferntial * 10;
    if (value > 100) {
      value = 100;
    }
  }
  const getColor = () => {
    if (value <= 20) return "bg-green-500";
    if (value <= 40) return "bg-lime-500";
    if (value <= 60) return "bg-yellow-300";
    if (value <= 70) return "bg-orange-400";
    if (value <= 85) return "bg-orange-500";
    return "bg-red-500";
  };

  return (
    <>
      <div className="w-full flex flex-wrap bg-white justify-center items-center text-center my-2 rounded-md">
        {data && (
          <>
            <h2 className="w-full text-center font-medium text-md py-2">
              Next 24 Hours from{" "}
              <span className="font-bold text-2xl">
                {DateFormatter(data?.time)}
              </span>
            </h2>
            <h2 className="w-full text-center font-medium text-lg py-2">
              Todays fluctuation{" "}
              <span className="font-bold text-2xl">
                {" "}
                {Math.round(HPAdifferntial * 100) / 100}
              </span>{" "}
              hPa
            </h2>
            <h2 className="w-full text-center font-medium text-lg py-2">
              Current Pressure:{" "}
              <span className="font-bold text-2xl">{data?.current}</span> hPa
            </h2>
            <h2 className="w-full text-center py-2 text-xl">
              Location{" "}
              <span className="font-bold text-2xl">{data?.zipcode}</span>
            </h2>
            <div className="h-15 w-full overflow-hidden">
              <div className="flex justify-between bg-emerald-200 h-auto p-1 text-lg font-extrabold">
                <span>1 hPa</span>
                <span>10 hPa</span>
              </div>
              <div className="bg-violet-200 w-full rounded-r-md shadow-lg">
                <div
                  className={`h-10 ${getColor()} transition-all shadow-md rounded-r-md`}
                  style={{ width: `${value}%` }}
                ></div>
              </div>
              <div className="w-auto flex justify-between lg:mx-10">
                <span className="text-center">
                  <div>Lowest:</div>
                  <div>{Math.round(data?.low * 100) / 100} hPA</div>
                </span>
                <span className="text-center">
                  <div>Average:</div>
                  <div>{Math.round(data?.low * 100) / 100} hPA</div>
                </span>
                <span className="text-center">
                  <div>Highest:</div>
                  <div>{Math.round(data?.low * 100) / 100} hPA</div>
                </span>
              </div>
            </div>
          </>
        )}
      </div>

      {HPAdifferntial > 5 && (
        <div className="w-full flex flex-col bg-white justify-center items-center text-center rounded-md">
          <span className="text-md rounded-lg m-2 p-2 text-rose-700/50 border-2 border-rose-700/50 bg-rose-700/20">
            !! It may be smart to take an anti-inflammatory today !!
          </span>
          <a
            rel="noopener noreferrer"
            target="_blank"
            href="https://www.healthline.com/health/headache/barometric-pressure-headache#:~:text=Migraine%20frequency%20increased%20on%20days,higher%20than%20the%20previous%20day."
            className="hover:font-bold text-center text-sm rounded-lg p-2 text-rose-700/50 underline"
          >
            &quot;Migraine frequency increased on days when the barometric
            pressure was lower by 5 hectopascals (hPa) than the previous
            day.&quot;
          </a>{" "}
          <a
            rel="noopener noreferrer"
            target="_blank"
            href="https://www.healthline.com/health/headache/barometric-pressure-headache#:~:text=Migraine%20frequency%20increased%20on%20days,higher%20than%20the%20previous%20day."
            className="rounded-lg m-2 p-2 text-xl text-rose-700/50"
          >
            Healthline.com
          </a>
        </div>
      )}
    </>
  );
}

export default Meter;
