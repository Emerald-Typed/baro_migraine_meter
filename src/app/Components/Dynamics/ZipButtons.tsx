"use client";
import { useEffect, useState } from "react";
import { getAllZipCodes } from "@/app/server/cookies";
import { useZipCode } from "@/app/Components/Dynamics/ZipContext";
import { expireFormat } from "@/app/Components/Helpers/DateFormatter";

import { MeterData } from "@/app/Components/dataCleanup/types";

function ZipButtons() {
  const [buttons, setButtons] = useState<MeterData[] | null>(null);
  const { selectedZipCode, updateSelectedZipCode } = useZipCode();
  useEffect(() => {
    const fetchData = async () => {
      try {
        const allZipcodes = await getAllZipCodes();
        setButtons(allZipcodes);
      } catch (error) {
        console.error("Error fetching zip codes:", error);
        return null;
      }
    };
    fetchData();
  }, [selectedZipCode]);
  return (
    <div className="flex w-full justify-center space-x-2 flex-row items-center">
      {buttons &&
        buttons.map((button, index) => (
          <button
            key={index}
            className={`bg-white p-1 hover:bg-gray-200 rounded-md text-center flex flex-col justify-center items-center`}
            onClick={() => updateSelectedZipCode(button.zipcode)}
          >
            <a>{button.zipcode} </a>
            <a>Expires: </a>
            <a>{expireFormat(button.timeStamp).time}</a>
            <a>{expireFormat(button.timeStamp).day}</a>
          </button>
        ))}
    </div>
  );
}
export default ZipButtons;
