"use client";
import { getRecentZip } from "@/app/server/cookies";
import { createContext, useContext, useEffect, useState } from "react";

type ZipCodeContextValueType = {
  selectedZipCode: string | undefined;
  updateSelectedZipCode: (data: string) => void;
};

const initialZipCodeContextValue: ZipCodeContextValueType = {
  selectedZipCode: undefined,
  updateSelectedZipCode: () => {},
};

const ZipCodeContext = createContext<ZipCodeContextValueType>(
  initialZipCodeContextValue
);

export const ZipCodeProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [selectedZipCode, setSelectedZipCode] = useState<string>();
  //METHOD SO I CAN UPDATE CONTEXT VALUE MANUALLY USED FOR ZIPBUTTONS
  const updateSelectedZipCode = (zip: string) => {
    setSelectedZipCode(zip);
  };

  const value: ZipCodeContextValueType = {
    selectedZipCode,
    updateSelectedZipCode,
  };
  //ASSUME MOST RECENT FORM ENTRY ON REFRESH
  useEffect(() => {
    const fetchData = async () => {
      try {
        const zip = await getRecentZip();
        if (zip) {
          updateSelectedZipCode(zip.zipcode);
        } else {
          console.log("No Zip Code found.");
          updateSelectedZipCode("00000");
        }
      } catch (error) {
        console.error("Error fetching zip code:", error);
      }
    };
    fetchData();
  }, []);

  return (
    <ZipCodeContext.Provider value={value}>{children}</ZipCodeContext.Provider>
  );
};

export const useZipCode = () => {
  return useContext(ZipCodeContext);
};
