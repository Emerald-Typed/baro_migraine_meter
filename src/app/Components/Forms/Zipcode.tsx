"use client";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import { useFormState } from "react-dom"; //react 19 useActionState
import ReCAPTCHA from "react-google-recaptcha";
import { ConvertandFetchData } from "@/app/server/ServerActions";
import { useZipCode } from "@/app/Components/Dynamics/ZipContext";
import { State } from "@/app/Components/dataCleanup/types";

const initialState: State = { state: "Idle", message: "Awaiting Actions" };

const ZipCodeForm = () => {
  //useFormState or useActionState allows (ie ConvertandFetchData)   fdfsdfsdfsdfsdfklsdfjsdlkfjsdkldserver action function on formAction
  const [Formstate, formAction] = useFormState(
    ConvertandFetchData,
    initialState
  );
  const [isVerified, setIsVerified] = useState(false);
  const [dynamicClass, setClassName] = useState("");
  const { selectedZipCode, updateSelectedZipCode } = useZipCode();
  const router = useRouter();
  const handleRecaptchaChange = (value: any) => {
    setIsVerified(!!value);
  };

  useEffect(() => {
    switch (Formstate.state) {
      case "Success":
        setClassName("disabled:bg-emerald-800");
        //this was what i needed, I changed the initial state from state: string to an object with {state: string, message: string to pass back formData from useFormState}
        updateSelectedZipCode(Formstate.message);
        router.push("/DashBoard");
        break;
      case "Idle":
        setClassName("disabled:bg-gray-400");
        break;
      default:
        setClassName("disabled:bg-red-600");
    }
  }, [Formstate]);

  return (
    <div className="w-full">
      <form
        className="w-full space-y-4 py-4 flex flex-col bg-white justify-center items-center text-center my-2 rounded-md"
        action={formAction}
      >
        <div className="flex justify-center items-center text-lg">
          <label className="text-center mr-2">Zip Code: </label>
          <input
            id="zipcode"
            type="text"
            name="zipcode"
            minLength={5}
            maxLength={5}
            defaultValue={selectedZipCode}
            className=" w-auto p-2 m-1 outline focus:outline-none focus:ring focus:ring-violet-400/75 text-center shadow-sm rounded-md"
          />
        </div>
        <div className="flex justify-center">
          <ReCAPTCHA
            sitekey="6LdeC-0pAAAAAO8V_o5IOMXUkJTkh455pJBKEJtp"
            onChange={handleRecaptchaChange}
          />
        </div>
        <div className="justify-center flex w-full">
          <button
            className={`${dynamicClass} bg-violet-400 p-4 text-white font-medium rounded-md w-1/2 md:w-1/4 enabled:hover:bg-violet-500`}
            type="submit"
            disabled={Formstate.state !== "Idle" || !isVerified}
          >
            {Formstate.state === "Idle"
              ? "Submit"
              : Formstate.state === "Success"
              ? "Success"
              : Formstate.state +
                `, Please refresh and try again // message: ` +
                Formstate.message}
          </button>
        </div>
      </form>
      {selectedZipCode && (
        <span className="w-full flex justify-center align-center">
          <a
            href={"/DashBoard"}
            className="text-center bg-violet-400 text-sm md:text-md p-2 text-white font-medium rounded-md enabled:hover:bg-violet-500"
          >
            View Last Search
          </a>
        </span>
      )}
    </div>
  );
};

export default ZipCodeForm;
