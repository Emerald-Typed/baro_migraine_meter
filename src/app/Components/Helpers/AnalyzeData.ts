import {
  MeterData,
  Timeline,
  twentyFourCast,
} from "@/app/Components/dataCleanup/types";

export function store24Cast(data: Timeline): twentyFourCast {
  const first24HoursData = data.hourly.slice(0, 24).map((entry) => ({
    time: entry.time,
    pressureSurfaceLevel: entry.values.pressureSurfaceLevel,
  }));
  return first24HoursData;
}

//consumes store24Cast output
export function analyzePressure(
  data: twentyFourCast
): Omit<MeterData, "zipcode" | "timeStamp"> {
  const pressureLevels = data.map((entry) => entry.pressureSurfaceLevel);
  const highPressure = Math.max(...pressureLevels);
  const lowPressure = Math.min(...pressureLevels);
  const averagePressure =
    pressureLevels.reduce((acc, curr) => acc + curr, 0) / pressureLevels.length;
  const current = data[0].pressureSurfaceLevel;
  return {
    time: data[0].time,
    high: highPressure,
    low: lowPressure,
    average: averagePressure,
    current: current,
  };
}
