const dateTimeString = "2024-05-30T10:30:00Z"; // Adjusted to match your desired time

export const DateFormatter = (dateString: string) => {
  const dateTime = new Date(dateString);
  // Format the date as MM/DD/YY
  const formattedDate = dateTime.toLocaleDateString("en-US", {
    month: "numeric",
    day: "numeric",
    year: "2-digit",
  });
  // Format the time as HH:MM
  const formattedTime = dateTime.toLocaleTimeString("en-US", {
    hour: "2-digit",
    minute: "2-digit",
  });
  const formattedDateTime = `${formattedDate} ${formattedTime}`;
  return formattedDateTime;
};

//split recently

export const expireFormat = (dateString: number) => {
  const dateTime = new Date(dateString);
  const expirationDate = new Date(dateTime.getTime() + 24 * 60 * 60 * 1000);
  const formattedTime = expirationDate.toLocaleTimeString("en-US", {
    hour: "2-digit",
    minute: "2-digit",
  });
  const formattedDay = expirationDate.toLocaleDateString("en-US", {
    month: "2-digit",
    day: "2-digit",
  });
  const exportExpire = {
    time: formattedTime,
    day: formattedDay,
  };
  return exportExpire;
};
