export async function zipToLatLong(zip: string) {
  const url = `https://nominatim.openstreetmap.org/search?postalcode=${zip}&country=us&format=json`;

  try {
    const response = await fetch(url, { cache: "no-store" });
    const data = await response.json();

    if (data && data.length > 0) {
      const latitude = parseFloat(data[0].lat); // Convert latitude to float
      const longitude = parseFloat(data[0].lon);
      return { latitude, longitude };
    } else {
      throw new Error("No location found for the ZIP code.");
    }
  } catch (error) {
    console.error("Error converting ZIP to lat/long:", error);
    return null;
  }
}
