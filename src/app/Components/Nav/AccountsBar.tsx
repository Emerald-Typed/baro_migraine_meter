import Link from "next/link";
import { SiKofi, SiGitlab, SiObsidian, SiLinkedin } from "react-icons/si";

export default function AccountsBar() {
  return (
    <>
      <div className="bg-gradient-to-r from-emerald-400 via-violet-400 to-purple-400 sticky z-50 top-0 w-full">
        <nav className="text-center items-center justify-center space-x-2 py-2 bg-emerald-400/75 lg:flex">
          <a
            href={"/"}
            className="block hover:text-violet-200 text-white font-light text-4xl font-semibold leading-none tracking-tight"
          >
            Barometric Migraine Meter{" "}
          </a>
          <a className="block font-medium px-2 md:text-xl sm:text-lg text-violet-900/60">
            by Emerald-Typed, Kyle Hipple
          </a>
        </nav>
      </div>
      <nav className="bg-violet-400/50 shadow-lg pt-1">
        <div className="mx-auto text-center sm:px-6 lg:px-8">
          <a className="text-white font-light mt-2  text-2xl font-semibold leading-none tracking-tight">
            My Accounts
          </a>
          <div className="lg:space-x-10 lg:flex lg:justify-center grid grid-cols-2 text-white pb-2">
            <Link
              href="https://gitlab.com/Emerald-Typed"
              rel="noopener noreferrer"
              target="_blank"
              className="inline-flex block bg-violet-400/75 items-center font-light rounded-md hover:font-medium hover:shadow-sm p-2 m-3"
            >
              <SiGitlab className="mr-1" />
              GitLab
            </Link>
            <Link
              href="https://www.linkedin.com/in/kyle-hipple"
              rel="noopener noreferrer"
              target="_blank"
              className="inline-flex block bg-violet-400/75 items-center font-light rounded-md hover:font-medium hover:shadow-sm p-2 m-3"
            >
              <SiLinkedin className="mr-1" />
              LinkedIn
            </Link>
            <Link
              href="https://ko-fi.com/emerald_typed"
              rel="noopener noreferrer"
              target="_blank"
              className="inline-flex block bg-violet-400/75 items-center font-light rounded-md hover:font-medium hover:shadow-sm p-2 m-3"
            >
              <SiKofi className="mr-1" />
              Kofi
            </Link>
            <Link
              href="https://emerald-typed.vercel.app/"
              rel="noopener noreferrer"
              target="_blank"
              className="inline-flex block bg-violet-400/75 items-center font-light rounded-md hover:font-medium hover:shadow-sm p-2 m-3"
            >
              <SiObsidian className="mr-1" />
              Portfolio
            </Link>
          </div>
        </div>
      </nav>
    </>
  );
}
