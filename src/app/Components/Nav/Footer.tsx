import Link from "next/link"
export default function Footer() {
  return (
    <div className="bg-gradient-to-r from-emerald-400 via-violet-400 to-purple-400">
      <div className="w-full bg-emerald-400/75 text-center text-lg font-semibold leading-8 text-white min-h-full">
        <Link
          className="text-2xl group"
          href={"https://gitlab.com/Emerald-Typed/baro_migraine_meter"}
          target="_blank"
          rel="noopener noreferrer"
        >
          <span className="text-violet-700/60 font-medium group-hover:text-violet-900/60 group-hover:font-bold">Repo Link</span> 🦖
          <span className="text-violet-700/60 font-medium  group-hover:text-violet-900/60 group-hover:font-bold"> for Project</span>
        </Link>
        <h2>Built with 💚</h2>
        <footer className="flex flex-col gap-2 sm:flex-row py-6 w-full shrink-0 items-center px-4 md:px-6 border-t max-w-7xl mx-auto">
          <p className="text-xs ">© 2024 Emerald Typed All rights reserved.</p>
          <nav className="sm:ml-auto flex gap-4 sm:gap-6">
            <Link
              className="text-xs hover:underline underline-offset-4"
              href="https://gitlab.com/Emerald-Typed/baro_migraine_meter/-/raw/main/LICENSE.txt"
            >
              Liscense Agreement
            </Link>
          </nav>
        </footer>
      </div>
    </div>
  )
}
