import data from "../../../../public/exampleAPIHourly.json";
import fs from "fs";
const formatJSONHourly = () => {
  const filteredData = data.timelines.hourly.map((item) => {
    return {
      time: item.time,
      pressureSurfaceLevel: item.values.pressureSurfaceLevel,
    };
  });
  const extractedData = {
    timelines: {
      hourly: [...filteredData],
    },
    location: { lat: 29.741093604166668, lon: -95.44962130548245 },
  };
  console.log(extractedData);
  fs.writeFile(
    "extractedData.json",
    JSON.stringify(extractedData, null, 2),
    (err) => {
      if (err) {
        console.error("Error writing JSON file:", err);
      } else {
        console.log("JSON file has been saved.");
      }
    }
  );
  return extractedData;
};

module.exports = formatJSONHourly;
