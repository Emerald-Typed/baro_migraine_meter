//output of AnalyzePressure
export type MeterData = {
  timeStamp: number;
  zipcode: string;
  time: string;
  current: number;
  high: number;
  low: number;
  average: number;
};
//for AnalyzePressure
export type twentyFourCast = forcastObject[];
type forcastObject = {
  time: string;
  pressureSurfaceLevel: number;
};
//useFormState intial state type
export type State = {
  state: string;
  message: string;
};
//For 24Cast consumed by AnalyzePressure ^^^
export type Timeline = {
  hourly: HourlyData[];
};
export type HourlyData = {
  time: string;
  values: PressureSurfaceLevelOnly;
};
//allows me to take the values object and only get the pressureSurfaceLevel only because sometimes other values return null
type PressureSurfaceLevelOnly = Pick<Values, "pressureSurfaceLevel">;
//helps avoid error if say cloudCeiling is null in less lines of code
export type Values = {
  [key: string]: number;
};

//experimenting with different ways to use types on large return objects

// export type Values = {
//   cloudBase: number;
//   cloudCeiling: number;
//   cloudCover: number;
//   dewPoint: number;
//   evapotranspiration: number;
//   freezingRainIntensity: number;
//   humidity: number;
//   iceAccumulation: number;
//   iceAccumulationLwe: number;
//   precipitationProbability: number;
//   pressureSurfaceLevel: number;
//   rainAccumulation: number;
//   rainAccumulationLwe: number;
//   rainIntensity: number;
//   sleetAccumulation: number;
//   sleetAccumulationLwe: number;
//   sleetIntensity: number;
//   snowAccumulation: number;
//   snowAccumulationLwe: number;
//   snowDepth: number;
//   snowIntensity: number;
//   temperature: number;
//   temperatureApparent: number;
//   uvHealthConcern: number;
//   uvIndex: number;
//   visibility: number;
//   weatherCode: number;
//   windDirection: number;
//   windGust: number;
//   windSpeed: number;
// };

// export type Values = {
//   cloudBase?: number | null;
//   cloudCeiling?: number | null;
//   cloudCover?: number | null;
//   dewPoint?: number | null;
//   evapotranspiration?: number | null;
//   freezingRainIntensity?: number | null;
//   humidity?: number | null;
//   iceAccumulation?: number | null;
//   iceAccumulationLwe?: number | null;
//   precipitationProbability?: number | null;
//   pressureSurfaceLevel: number;
//   rainAccumulation?: number | null;
//   rainAccumulationLwe?: number | null;
//   rainIntensity?: number | null;
//   sleetAccumulation?: number | null;
//   sleetAccumulationLwe?: number | null;
//   sleetIntensity?: number | null;
//   snowAccumulation?: number | null;
//   snowAccumulationLwe?: number | null;
//   snowDepth?: number | null;
//   snowIntensity?: number | null;
//   temperature?: number | null;
//   temperatureApparent?: number | null;
//   uvHealthConcern?: number | null;
//   uvIndex?: number | null;
//   visibility?: number | null;
//   weatherCode?: number | null;
//   windDirection?: number | null;
//   windGust?: number | null;
//   windSpeed?: number | null;
// };
