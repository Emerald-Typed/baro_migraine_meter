"use client";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import { getRecentZip } from "@/app/server/cookies";
import Meter from "@/app/Components/Dynamics/Meter";
import ZipButtons from "@/app/Components/Dynamics/ZipButtons";
import { ForecastGraph } from "@/app/Components/Dynamics/24Cast";

export default function Dashboard() {
  const router = useRouter();
  //PUSH HOME TO FORM IF NO COOKIE EXISTS TO DRAW DATA FROM FOR DASHBOARD
  useEffect(() => {
    const fetchData = async () => {
      const hasZip = await getRecentZip();
      if (!hasZip) {
        router.push("/");
      }
    };
    fetchData();
  }, []);
  return (
    <main className="flex w-auto min-h-screen flex-col items-center justify-between">
      <div className="p-2 w-80%">
        <h1 className="text-5xl font-bold text-center text-violet-400 my-4">
          Barometric Migraine Meter
        </h1>
        <p className="text-center text-lg text-violet-400">Todays Dashboard</p>
        <ZipButtons />
        <Meter />
        <ForecastGraph />
        <div className="justify-center space-x-6 flex">
          <a
            href={"/"}
            className="disabled:bg-emerald-800 bg-violet-400 p-2 text-center my-5 text-white font-medium rounded-md w-1/2 md:w-1/4 enabled:hover:bg-violet-500"
          >
            Resubmit Form
          </a>
        </div>
      </div>
    </main>
  );
}
