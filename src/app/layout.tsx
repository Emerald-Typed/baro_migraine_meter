import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import AccountsBar from "@/app/Components/Nav/AccountsBar";
import Footer from "@/app/Components/Nav/Footer";
const inter = Inter({ subsets: ["latin"] });
import { ZipCodeProvider } from "@/app/Components/Dynamics/ZipContext";

export const metadata: Metadata = {
  title: "Emerald Typed Barometric Migraine Meter",
  description:
    "Basic App to take in barometric weather data and check for fluctuations greater than 5 hectopascals using tomorrow.io API",
  icons: "/diamondE.png",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <AccountsBar />
        <div className="min-h-screen flex flex-col w-full overflow-auto">
          <ZipCodeProvider>{children}</ZipCodeProvider>
        </div>
        <Footer />
      </body>
    </html>
  );
}
