import ZipCodeForm from "@/app/Components/Forms/Zipcode";
export default function Home() {
  return (
    <main className="flex w-auto min-h-screen flex-col items-center justify-between">
      <div className="p-2 w-80%">
        <h1 className="text-5xl font-bold text-center text-violet-400">
          Barometric Migraine Meter
        </h1>
        <p className="text-center text-lg text-violet-400">
          A simple tool to track your migraines and the barometric pressure
        </p>
        <ZipCodeForm />
      </div>
    </main>
  );
}
