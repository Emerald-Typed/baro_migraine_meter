"use server";
import { cookies } from "next/headers";
//Testing Sans a call haha
// import Data from "@/app/Components/dataCleanup/RealData.json";
import {
  analyzePressure,
  store24Cast,
} from "@/app/Components/Helpers/AnalyzeData";
import { zipToLatLong } from "@/app/Components/Helpers/ZipToLatLong";
import { MeterData, State } from "@/app/Components/dataCleanup/types";
import { TomorrowIOCall } from "./TomorrowAPI";

export const ConvertandFetchData = async (
  previousState: State,
  formData: FormData
) => {
  const userZipcode = formData.get("zipcode") as string;
  // would usually use try catch with throw error but I am returning
  // state message that i can feed back to user on client side using
  // useFormState/useActionState to use server fucntions from a client form
  // I try use console.error only on server its easy to view in production for myself
  // but for client useFormState allows me to pass stateful visual messages to patrons easier.
  if (!userZipcode) {
    return { state: "Error", message: "Missing Zipcode" };
  }
  const location = await zipToLatLong(userZipcode);
  if (!location) {
    return { state: "Error", message: "Invalid Zipcode" };
  }
  const weatherData = await TomorrowIOCall(location);
  if (!weatherData || !weatherData.timelines) {
    return { state: "Error", message: "Failed to Fetch Weather Data" };
  }
  const twentyFourCast = store24Cast(weatherData.timelines);
  const { time, high, low, average, current } = analyzePressure(twentyFourCast);

  const meterData: MeterData = {
    timeStamp: Date.now(),
    zipcode: userZipcode,
    time: time,
    high: high,
    low: low,
    average: average,
    current: current,
  };
  //cookie expiration
  const oneDay = 24 * 60 * 60 * 1000;
  const expire = Date.now() + oneDay;
  //for graphing data later also I would usally use db not stringify for speed but it tis a small project
  cookies().set(`${userZipcode}-FourCast`, JSON.stringify(twentyFourCast), {
    expires: expire,
  });
  //data for meter
  cookies().set(`Data-${userZipcode}`, JSON.stringify(meterData), {
    expires: expire,
  });
  return {
    state: "Success",
    message: `${userZipcode}`,
  };
};
