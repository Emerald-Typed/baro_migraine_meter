"use server";

const apiKey = process.env.TOMAPIKEY;

type convertedLocation = {
  latitude: number;
  longitude: number;
};

export const TomorrowIOCall = async (data: convertedLocation) => {
  const options = { method: "GET", headers: { accept: "application/json" } };
  try {
    const response = await fetch(
      `https://api.tomorrow.io/v4/weather/forecast?location=${data.latitude},${data.longitude}&timesteps=1h&units=metric&apikey=${apiKey}`,
      { cache: "no-store", ...options } //prevents auto-cache in production
    );
    if (!response.ok) {
      throw new Error("Network response was not ok, it was not okay, okay lol");
    }
    const result = await response.json();
    return result;
  } catch (error) {
    console.error("Error fetching weather data:", error);
    return null;
    //message in serverside generated for client
  }
};
