"use server";
import { cookies } from "next/headers";
//sidenote to self: falsy terminal.integrated.shellIntegration.enabled temporarily for bash speed with 6/8/24 windowsupdate till resolved
import { MeterData } from "@/app/Components/dataCleanup/types";

//used to check context on load
export const getRecentZip = async () => {
  const cookiesArray = cookies()
    .getAll()
    .filter((cookie) => cookie.name.startsWith("Data-"));

  const sortByRecent = cookiesArray
    .map((cookie) => {
      const cookieData = JSON.parse(cookie.value);
      return {
        ...cookieData,
      } as MeterData;
    })
    .sort((a, b) => b.timeStamp - a.timeStamp);

  let mostRecentZip = null;
  if (sortByRecent) {
    mostRecentZip = sortByRecent[0];
    return mostRecentZip;
  } else {
    console.error("No cookies found.");
    return null;
  }
};

//load in data
export const getData = (zip: string) => {
  //fix later to zip timestamp zips are only overwritten by hour
  const dataCookie = cookies().get(`Data-${zip}`);
  if (dataCookie) {
    const data = JSON.parse(dataCookie.value);
    return data;
  } else {
    console.error("No cookies found.");
  }
};
//buttons
export const getAllZipCodes = async () => {
  const cookiesArray = cookies()
    .getAll()
    .filter((cookie) => cookie.name.startsWith("Data-"));
  const sortByRecent = cookiesArray
    .map((cookie) => {
      const cookieData = JSON.parse(cookie.value);
      return {
        ...cookieData,
      } as MeterData;
    })
    .sort((a, b) => b.timeStamp - a.timeStamp);
  if (sortByRecent.length > 0) {
    return sortByRecent;
  } else {
    console.error("Need multiple cookies found /none or 1 exist.");
    return null;
  }
};

//data for future graph
export const getTwentyFourCast = async (zip: string) => {
  const twentyFourCast = cookies().get(`${zip}-FourCast`);
  if (twentyFourCast) {
    try {
      const parsedData = JSON.parse(twentyFourCast.value);
      return parsedData;
    } catch (error) {
      console.error("Error parsing JSON:", error);
    }
  } else {
    return null;
  }
};
